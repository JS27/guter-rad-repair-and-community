import {Component, OnInit} from '@angular/core';
import {Anleitung} from '../anleitung';
import {AnleitungService} from '../anleitung.service';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-anleitung',
  templateUrl: './anleitung.component.html',
  styleUrls: ['./anleitung.component.css']
})
export class AnleitungComponent implements OnInit {
  searchText;
  private sessionUser: User|null;
  neueAnleitung?: Anleitung = {
    title: '',
    imgURL: '',
    text: '',
    author: ''
  };
  anleitungen: Anleitung[];
  selectedAnleitung: Anleitung;
  showExtended = true;

  constructor(private anleitungService: AnleitungService, private http: HttpClient,
              private securityService: SecurityService, private router: Router) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.anleitungen = this.anleitungService.getAnleitungen();
    console.log(this.anleitungen);
  }

  onSelecet(anleitung: Anleitung): void {
    this.selectedAnleitung = anleitung;
  }

  // hier wird eine neue Anleitung, die von eingeloggten Usern erstellt wurde, an die Datenbank übertragen
  writeAnleitung() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.neueAnleitung.author = this.sessionUser.username;
    this.anleitungService.saveAnleitung(this.neueAnleitung);
    this.router.navigate(['/']);
  }

}
