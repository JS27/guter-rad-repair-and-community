import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionUserComponent } from './session-user/session-user.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { AnleitungComponent } from './anleitung/anleitung.component';
import { KarteComponent } from './karte/karte.component';
import { StartseiteComponent } from './startseite/startseite.component';
import { ForumComponent } from './forum/forum.component';
import { AnleitungDetailsComponent } from './anleitung-details/anleitung-details.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CarouselBasicComponent } from './carousel-basic/carousel-basic.component';
import { RegistrierenComponent } from './registrieren/registrieren.component';
import { ForumBeitragComponent } from './forum-beitrag/forum-beitrag.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


/*
 npm i ng2-search-filter --save
 um Search-Bar zu nutzen
*/


@NgModule({
  declarations: [
    AppComponent,
    SessionUserComponent,
    LoginComponent,
    AnleitungComponent,
    KarteComponent,
    StartseiteComponent,
    ForumComponent,
    AnleitungDetailsComponent,
    CarouselBasicComponent,
    RegistrierenComponent,
    // CommentComponent,
    ForumBeitragComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    Ng2SearchPipeModule
    // NgbCollapseModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
