import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ForumBeitrag} from '../forumBeitrag';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {Anleitung} from '../anleitung';
import {ForumService} from '../forum.service';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {
  searchText;
  forumBeitraege: ForumBeitrag[];

  private sessionUser: User|null;
  neuerBeitrag?: ForumBeitrag = {title: '', name: '', text: '', createdAt: null};
  private data;
  selectedBeitrag: ForumBeitrag;

  constructor(private http: HttpClient, private securityService: SecurityService, private forumService: ForumService) { }

  ngOnInit() {
    this.forumBeitraege = this.forumService.getBeitraege();
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.http.get<ForumBeitrag[]>('/api/forum')
      .subscribe(forumbeitraege => this.forumBeitraege = forumbeitraege);
    this.neuerBeitrag = {title: 'Titel', name: this.neuerBeitrag.name , text: 'Text', createdAt: null};
  }
  onSelecet(forumbeitrag: ForumBeitrag): void {
    this.selectedBeitrag = forumbeitrag;
  }

  writeEintrag() {
    this.neuerBeitrag.name = this.sessionUser.username;
    this.http.post<ForumBeitrag[]>('/api/forum', this.neuerBeitrag)
      .subscribe(forumbeitraege => this.forumBeitraege = forumbeitraege);
    this.neuerBeitrag = {title: 'Titel', name: this.neuerBeitrag.name , text: 'Text', createdAt: null};
  }
}
