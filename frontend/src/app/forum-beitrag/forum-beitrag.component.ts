import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ForumBeitrag} from '../forumBeitrag';
import {ForumService} from '../forum.service';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {Comment} from './comment';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-forum-beitrag',
  templateUrl: './forum-beitrag.component.html',
  styleUrls: ['./forum-beitrag.component.css']
})
export class ForumBeitragComponent implements OnInit {
  hilfsid: string;
  private sessionUser: User | null;
  private comments: Comment[] = [];
  neuerComment?: Comment = {
    text: '',
    beitragsId: null
  };
  data;
  private forumBeitrag: ForumBeitrag = {
    id: 0,
    title: '',
    name: '',
    text: '',
  };
  showExtended = true;
  // beitragUpdate: ForumBeitrag;

  constructor(private activatedRoute: ActivatedRoute, private forumService: ForumService,
              private securityService: SecurityService, private router: Router, private http: HttpClient) {
  }

  ngOnInit() {
    this.hilfsid = this.activatedRoute.snapshot.paramMap.get('id');
    this.http.get<Comment[]>('api/forum/comments').subscribe(f => this.comments = f);
    this.getBeitrag();
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u);
  }

  getBeitrag() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.forumBeitrag = this.forumService.getBeitrag(+(id));
  }

  getBeitragId(): number {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    return id;
  }

  loescheBeitrag() {
    if (this.sessionUser.username === this.forumBeitrag.name) {
      const id = +this.activatedRoute.snapshot.paramMap.get('id');
      this.forumService.loescheBeitrag(Number(id));
    }
    this.goToForum();
  }

  goToForum() {
    this.router.navigate(['forum']);
  }

  writeKommentar() {
    this.neuerComment.beitragsId = +(this.activatedRoute.snapshot.paramMap.get('id'));
    this.http.post<Comment[]>('/api/forum/comments', this.neuerComment)
      .subscribe(comments => this.comments = comments);
    this.neuerComment = {text: 'Text', beitragsId: null};
  }

  // bearbeiteBeitrag() {
  //   this.getBeitrag();
  //   this.beitragUpdate = {id: 0, title: 'Titel', name: 'name', text: 'Text', createdAt: null};
// }
}
