import {Injectable} from '@angular/core';
import {Anleitung} from './anleitung';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AnleitungService {
  private anleitungen: Anleitung[] = [];
  private neueAnleitung: Anleitung = {title: '', imgURL: null, text: '', author: '', id: null};
  private data;

  constructor(private http: HttpClient) {
  }

  getAnleitungen(): Anleitung[] {
    this.http.get<Anleitung[]>('/api/anleitungen').subscribe(anleitungen => this.anleitungen = anleitungen);
    return this.anleitungen;
    // Add to local storage
    // localStorage.setItem('anleitungen', JSON.stringify(this.anleitungen));

  }

  getAnleitungDetails(id: number): Anleitung {
    return this.anleitungen.find(a => a.id === id);
  }
  loescheAnleitung(id: number) {
    this.http.post('/api/anleitungen/delete', id).subscribe(p => this.data = p);
  }

  saveAnleitung(anleitung: Anleitung) {
    this.http.post<Anleitung[]>('/api/anleitungen/', anleitung)
      .subscribe(anleitungen => this.anleitungen = anleitungen);
  }
}
