export interface Anleitung {
    title: string;
    imgURL?: string;
    text: string;
    author?: string;
    id?: number;
}
