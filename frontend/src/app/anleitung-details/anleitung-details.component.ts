import { Component, OnInit } from '@angular/core';
import {Anleitung} from '../anleitung';
import {ActivatedRoute, Router} from '@angular/router';
import {AnleitungService} from '../anleitung.service';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-anleitung-details',
  templateUrl: './anleitung-details.component.html',
  styleUrls: ['./anleitung-details.component.css']
})
export class AnleitungDetailsComponent implements OnInit {
  anleitung: Anleitung;
  anleitungen: Anleitung[];
  private sessionUser: User|null;
  data;
  ueberarbeiteteAnleitung: Anleitung = {
    title: '',
    imgURL: '',
    text: '',
  };

  constructor(
    private activatedRoute: ActivatedRoute, private anleitungService: AnleitungService,
    private securityService: SecurityService, private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.getAnleitungDetails();
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }
  getAnleitungDetails() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.anleitung = this.anleitungService.getAnleitungDetails(Number(id));
  }

  loescheAnleitung() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.anleitungService.loescheAnleitung(Number(id));
    this.router.navigate(['/']);
    this.anleitungService.getAnleitungen();
  }
  saveAnleitung() {
    this.http.post<Anleitung>('/api/anleitungen/bearbeiten', this.ueberarbeiteteAnleitung)
      .subscribe(a => this.ueberarbeiteteAnleitung = a);
    // // this.ueberarbeiteteAnleitung = {title: 'Titel', imgURL: 'Text', text: 'Text'};
    // const id = +this.activatedRoute.snapshot.paramMap.get('id');
    // this.anleitungService.getAnleitungDetails(Number(id));
    this.router.navigate(['/']);
    console.log(this.ueberarbeiteteAnleitung);
  }

}
