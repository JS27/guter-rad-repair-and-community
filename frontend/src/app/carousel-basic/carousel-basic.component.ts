import { Component, OnInit } from '@angular/core';
import {AnleitungService} from '../anleitung.service';
import {Anleitung} from '../anleitung';

@Component({
  selector: 'app-carousel-basic',
  templateUrl: './carousel-basic.component.html',
  styleUrls: ['./carousel-basic.component.css']
})
export class CarouselBasicComponent implements OnInit {
  // images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  images = ['https://cdn.pixabay.com/photo/2016/11/19/12/15/bicycle-1838972_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/05/07/09/03/gear-2291916_1280.jpg',
    'https://cdn.pixabay.com/photo/2013/07/13/13/43/racing-bicycle-161449_1280.png'];
   anleitungen: Anleitung[];
   imagesArray: string[];
   titles = ['Kette wechseln', 'Schaltung einstellen', 'Platten Reifen reparieren'];

  constructor(private anleitungService: AnleitungService) {
  }

  ngOnInit() {
     this.anleitungen = this.anleitungService.getAnleitungen();
     // this.fillArray();
  }
  // fillArray(): string[] {
  //   for (const e of this.anleitungen) {
  //     this.imagesArray.push(e.title);
  //   }
  //   console.log(this.imagesArray);
  //   return this.imagesArray;
  // }
}

