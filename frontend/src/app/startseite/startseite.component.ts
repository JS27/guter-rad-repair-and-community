import { Component, OnInit } from '@angular/core';
import {AnleitungService} from '../anleitung.service';

@Component({
  selector: 'app-startseite',
  templateUrl: './startseite.component.html',
  styleUrls: ['./startseite.component.css']
})
export class StartseiteComponent implements OnInit {

  constructor(private anleitungService: AnleitungService) { }

  ngOnInit() {
  }

}
