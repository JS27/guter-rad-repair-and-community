import {Injectable} from '@angular/core';
import {ForumBeitrag} from './forumBeitrag';
import {HttpClient} from '@angular/common/http';
import {User} from './user';


@Injectable({
  providedIn: 'root'
})
export class ForumService {
  private forumBeitraege: ForumBeitrag[] = [];
  private forumBeitraege2: ForumBeitrag[] = [];
  private beitragLIST: ForumBeitrag[];

  private sessionUser: User | null;
  private data;
  private neuerEintrag: ForumBeitrag = {id: null, title: '', name: '', text: '', createdAt: null};
  private forumBeitrag: ForumBeitrag = {
    id: 0,
    name: '',
    title: '',
    text: '',
  };

  constructor(private http: HttpClient) {
  }

  getBeitraege(): ForumBeitrag[] {
    this.http.get<ForumBeitrag[]>('/api/forum').subscribe(beitraege => {
      this.forumBeitraege = beitraege;
    });
    return this.forumBeitraege;
  }

  getBeitrag(id: number): ForumBeitrag {
    return this.forumBeitraege.find(a => a.id === id);
  }

  loescheBeitrag(id: number) {
    this.http.post('/api/forum/delete', id).subscribe(p => this.data = p);
  }
}


