import {Component, OnInit} from '@angular/core';
import {SessionUserComponent} from './session-user/session-user.component';
import {User} from './user';
import {ActivatedRoute} from '@angular/router';
import {AnleitungService} from './anleitung.service';
import {SecurityService} from './security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'frontend';
  navbarOpen = false;
  private sessionUser: User|null;

  constructor(private securityService: SecurityService) { }
  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}
