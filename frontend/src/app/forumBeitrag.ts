

export interface ForumBeitrag {
  id?: number;
  title: string;
  name: string;
  text: string;
  createdAt?;
}
