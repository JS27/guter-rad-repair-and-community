import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AnleitungComponent} from './anleitung/anleitung.component';
import {LoginComponent} from './login/login.component';
import {StartseiteComponent} from './startseite/startseite.component';
import {KarteComponent} from './karte/karte.component';
import {ForumComponent} from './forum/forum.component';
import {AnleitungDetailsComponent} from './anleitung-details/anleitung-details.component';
import {CarouselBasicComponent} from './carousel-basic/carousel-basic.component';
import {RegistrierenComponent} from './registrieren/registrieren.component';
import {ForumBeitragComponent} from './forum-beitrag/forum-beitrag.component';

const routes: Routes = [
  {path: 'anleitungen', component: AnleitungComponent},
  {path: 'anleitungen/:id', component: AnleitungDetailsComponent},
  {path: 'login', component: LoginComponent},
  {path: '', component: StartseiteComponent},
  {path: 'forum', component: ForumComponent},
  {path: 'karte', component: KarteComponent},
  {path: 'registrieren', component: RegistrierenComponent},
  {path: 'forum/:id', component: ForumBeitragComponent}
  // {path: '', component: CarouselBasicComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
