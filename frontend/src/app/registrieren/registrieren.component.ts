import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NeuerUser} from './registrieren';
import {User} from '../user';

@Component({
  selector: 'app-registrieren',
  templateUrl: './registrieren.component.html',
  styleUrls: ['./registrieren.component.css']
})
export class RegistrierenComponent implements OnInit {
  sessionUser: User | null = null;
  neuerUser: NeuerUser = {
    username: '', email: '', password1: '', password2: ''
  }
  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
  }
  public registrieren() {
    this.httpClient.post('/api/registrieren', this.neuerUser).subscribe();
    // this.neuerUser = { username: '', email: '', password1: '', password2: ''};
  }

}
