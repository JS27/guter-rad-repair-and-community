package de.awacademy.tollesprojekt.backend.anleitungen;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnleitungRepository extends JpaRepository<Anleitung, Long> {
    List<Anleitung> findAll();
}
