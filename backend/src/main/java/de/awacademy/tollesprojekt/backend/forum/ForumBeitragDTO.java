package de.awacademy.tollesprojekt.backend.forum;


public class ForumBeitragDTO {

    private long id;
    private String title;
    private String name;
    private String text;

    public ForumBeitragDTO(String title, String name, String text, long id) {
        this.title = title;
        this.name = name;
        this.text = text;
        this.id = id;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

