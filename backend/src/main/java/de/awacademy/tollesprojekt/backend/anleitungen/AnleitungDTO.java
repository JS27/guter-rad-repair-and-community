package de.awacademy.tollesprojekt.backend.anleitungen;


public class AnleitungDTO {

    private String title;
    private String imgURL;
    private String text;
    private String author;
    private Long id;

    public AnleitungDTO(String title, String imgURL, String text,  String author, Long id) {
        this.title = title;
        this.imgURL = imgURL;
        this.text = text;
        this.author = author;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getId() {
        return id;
    }
}
