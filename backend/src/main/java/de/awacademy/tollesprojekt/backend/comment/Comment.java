package de.awacademy.tollesprojekt.backend.comment;

import de.awacademy.tollesprojekt.backend.forum.ForumBeitrag;
import de.awacademy.tollesprojekt.backend.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Comment {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private User user;

    private String text;
    private Instant postedAt;
    private long beitragsId;

    public Comment(String text, long beitragsId) {
        this.text = text;
        this.beitragsId = beitragsId;
    }

    public long getBeitragsId() {
        return beitragsId;
    }



    @ManyToOne
    private ForumBeitrag beitrag;

    public Comment(User user, String text, Instant postedAt,ForumBeitrag beitrag) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
        this.beitrag = beitrag;
    }

    public Comment() {
    }

    public Comment(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public ForumBeitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(ForumBeitrag beitrag) {
        this.beitrag = beitrag;
    }

}
