package de.awacademy.tollesprojekt.backend.security;

public class UserDTO {

    private String username;

    public UserDTO(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
