package de.awacademy.tollesprojekt.backend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/api/registrieren")
    public void register (@RequestBody RegistrationDTO registrationDTO) {
        userService.register(registrationDTO.getUsername(),registrationDTO.getEmail(), registrationDTO.getPassword1());

    }




}
