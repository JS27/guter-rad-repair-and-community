package de.awacademy.tollesprojekt.backend.forum;

import de.awacademy.tollesprojekt.backend.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;

@Entity
public class ForumBeitrag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty
    private String title;
    private String name;

    @NotEmpty
    private String text;

    private Instant createdAt;

    @ManyToOne
    private User user;

    public ForumBeitrag() {
    }

    public ForumBeitrag(String title, String name, String text) {
        this.title = title;
        this.name = name;
        this.text = text;
        this.createdAt = Instant.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }
}
