package de.awacademy.tollesprojekt.backend.anleitungen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

@RestController
public class AnleitungenController {

    @Autowired
    private AnleitungRepository anleitungenRepository;

    @GetMapping("/api/anleitungen")
    public List<AnleitungDTO> lesen() {
        List<AnleitungDTO> response = new LinkedList<>();

        for (Anleitung anleitung : anleitungenRepository.findAll()) {
            response.add(new AnleitungDTO(anleitung.getTitle(), anleitung.getImgURL(), anleitung.getText(), anleitung.getAuthor(), anleitung.getId()));
        }

        return response;
    }

    @PostMapping("/api/anleitungen")
    public List<AnleitungDTO> schreiben(@RequestBody AnleitungDTO anleitungDTO) {
        anleitungenRepository.save(new Anleitung(anleitungDTO.getTitle(), anleitungDTO.getImgURL(), anleitungDTO.getText(), anleitungDTO.getAuthor()));
        return lesen();
    }

    @PostMapping("/api/anleitungen/bearbeiten")
    public void bearbeiten(@RequestBody Anleitung anleitung) {
        long id2 = anleitung.getId();
        Anleitung anleitungAlt = anleitungenRepository.findById(id2).get();
        anleitungAlt.setText(anleitung.getText());
        anleitungenRepository.save(anleitungAlt);
    }

    @PostMapping("/api/anleitungen/delete")
    public void anleitungLöschen(@RequestBody long id) {
        Anleitung anleitung = anleitungenRepository.findById(id).get();
//        if(anleitung.getAuthor() != sessionUser) {
//            throw new IllegalArgumentException("Dieser User darf die Anleitung nicht löschen");
//        }
        anleitungenRepository.delete(anleitung);
    }

}
