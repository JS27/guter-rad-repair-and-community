package de.awacademy.tollesprojekt.backend.comment;

public class CommentDTO {
    private String text;
    private long beitragsId;

    public CommentDTO(String text, long beitragsId) {

        this.text = text;
        this.beitragsId = beitragsId;
    }

    public String getText() {
        return text;
    }

    public long getBeitragsId() {
        return beitragsId;
    }
}
