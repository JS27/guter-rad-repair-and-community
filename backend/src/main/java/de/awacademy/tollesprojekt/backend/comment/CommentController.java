package de.awacademy.tollesprojekt.backend.comment;

import de.awacademy.tollesprojekt.backend.forum.ForumBeitragRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;
    private ForumBeitragRepository forumBeitragRepository;




    public CommentController(CommentRepository commentRepository ) {
        this.commentRepository = commentRepository;
    }

    public CommentController() {
    }

    @GetMapping("/api/forum/comments")
    public List<CommentDTO> readComment() {
//        Comment comment = null;

        List<CommentDTO> response = new LinkedList<>();

        for (Comment c : commentRepository.findAll()) {
            response.add(new CommentDTO ( c.getText (), c.getBeitragsId()));
        }

        return response;
    }
    @PostMapping("/api/forum/comments")
    public List<CommentDTO> schreiben(@RequestBody CommentDTO commentDTO) {
        commentRepository.save(new Comment (commentDTO.getText(),commentDTO.getBeitragsId()));
       return readComment();
    }
//
//    @PostMapping("/api/forum/{id}/delete")
//    public String commentLoeschen(@RequestBody long id) {
//        Comment comment = commentRepository.findById(id).get();
//        commentRepository.delete(comment);
//        return "redirect:/api/forum";
//    }



}
