package de.awacademy.tollesprojekt.backend;

import de.awacademy.tollesprojekt.backend.anleitungen.Anleitung;
import de.awacademy.tollesprojekt.backend.anleitungen.AnleitungRepository;
import de.awacademy.tollesprojekt.backend.comment.Comment;
import de.awacademy.tollesprojekt.backend.comment.CommentRepository;
import de.awacademy.tollesprojekt.backend.forum.ForumBeitrag;
import de.awacademy.tollesprojekt.backend.forum.ForumBeitragRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class BackendApplication {

    private final AnleitungRepository anleitungRepository;
    private final ForumBeitragRepository forumBeitragRepository;
    private final CommentRepository commentRepository;

    @Autowired
        public BackendApplication(AnleitungRepository anleitungRepository, ForumBeitragRepository forumBeitragRepository, CommentRepository commentRepository) {
            this.anleitungRepository = anleitungRepository;
            this.forumBeitragRepository = forumBeitragRepository;
            this.commentRepository = commentRepository;
    }
        public static void main(String[] args) {
            SpringApplication.run(BackendApplication.class, args);
        }
        @PostConstruct
        public void dummyData() {
            if (anleitungRepository.count() == 0) {
                anleitungRepository.save(new Anleitung("Neue Fahrradkette einbauen", "https://image.shutterstock.com/image-photo/bicycle-parts-rear-wheel-brake-600w-366933860.jpg", "Legen Sie die offene Kette vorne und hinten auf das kleinste Ritzel, so können Sie später die Kette bei geringster Spannung schließen. " +
                        "\n Führen Sie die Fahrradkette unter Beachtung des richtigen Verlaufs durch den Schaltkäfig: " +
                        "Einführen der Kette von unten hinter der unteren Schaltrolle. Kette über die obere Schaltrolle legen. " +
                        "Kette von hinten über das kleinste Ritzel legen.\n" +
                        "        Kette durch den Umwerfer führen.\n" +
                        "        Kette auf das große Kettenblatt legen.\n" +
                        "        Die Kette darf die Führungsröllchen nicht berühren.\n" +
                        "\n" +
                        "    Schließen Sie Ihre neue Kette, am besten mit einem Kettenschloss. Dieses erleichtert Ihnen mögliche spätere Nachfolgearbeiten.", "Admin"));
                anleitungRepository.save(new Anleitung("Schaltung einstellen", "https://cdn.pixabay.com/photo/2017/05/07/09/03/gear-2291916_1280.jpg", "1. Bevor Sie den Umwerfer einstellen, sollten Sie überprüfen, ob das äußere Leitblech des Umwerfers parallel zum großen Kettenblatt steht. <br>2. Der Abstand zwischen der Unterkante des äußeren Leitblechs und den Zähnen des großen Kettenblatts, sollte 1-3 Millimeter betragen.<br> 3. An der Unterseite aktueller Shimano-Schalthebel befindet sich der sogenannte Modus-Wandler. Je nach Art der Kurbel sollte er auf 2- oder 3-fach eingestellt sein. <br> 4. Schalten Sie zur Einstellung der unteren Begrenzung aufs kleine Kettenblatt und größtes Ritzel. <br> 5. Dann stellen Sie mit der L-Schraube das innere Leitblech so ein, dass es die Kette gerade nicht berührt. Schalten Sie aufs große Kettenblatt und kleinstes Ritzel.<br> 6. Justieren Sie nun mit der H-Schraube die äußere Begrenzung des Umwerfers. Die Kette soll einen minimalen Abstand zum äußeren Leitblech aufweisen.<br> 7. Als letztes stellen Sie die Zugspannung an der entsprechenden Schraube am Schalthebel ein. Drehen Sie hierfür die Zugspannungsschraube so weit, dass die Kette das Außenleitblech gerade nicht berührt.", "Admin"));
                anleitungRepository.save(new Anleitung("Licht hinten", "https://cdn.pixabay.com/photo/2013/07/13/13/43/racing-bicycle-161449_1280.png", "1. blblblblblblblblblblb 2. blnldöjoöjföaw", "Admin"));
                anleitungRepository.save(new Anleitung("Licht vorne", "https://cdn.pixabay.com/photo/2016/11/14/03/48/bicycle-1822528_1280.jpg", "1. blblblblblblblblblblb 2. blnldöjoöjföaw", "Admin"));
                anleitungRepository.save(new Anleitung("Platten Reifen reparieren", "https://cdn.pixabay.com/photo/2016/09/28/18/27/frog-1701111_1280.jpg", "Mit speziellen Reifenhebern – am besten aus Kunststoff und ohne scharfe Kanten – wird der Mantel an einer Seite des Reifens aus der Felge gehebelt. Sobald der Mantel aus der Felge gehoben ist, zieht man ihn mit Hilfe eines zweiten Reifenhebers nach und nach ab. Wenn er an einer Stelle nur noch schwer heruntergeht, versucht man es besser in der anderen Richtung. Wichtig ist es nämlich, den Schlauch nicht noch mehr zu beschädigen. Jetzt kann der Schlauch herausgenommen werden. Damit das Ventil durch die Öffnung in der Felge passt, muss man vorher natürlich die Ventilkappe und die Mutter, die das Ventil an der Felge fixiert, abschrauben. Damit man sieht, wo die Luft entweicht, muss auch welche im Schlauch sein. Deswegen ist erst einmal pumpen angesagt. Manchmal sieht oder hört man schnell, wo das Loch ist. Wenn man den Reifen nah ans Ohr hält, spürt man den Luftstrom oft auch an der Wange. Ihn in Wasser unterzutauchen und zu schauen, ob Blasen aufsteigen, funktioniert auch, das ist aber nicht die beste Methode. Die schadhafte Stelle kann man mit einem Stift markieren. Falls man den Schlauch zur Seite legt, muss man damit nicht erst wieder nach dem Loch suchen. Zur Vorbereitung für den Flicken wird der Schlauch an der markierten Stelle aufgeraut.", "Admin"));
                forumBeitragRepository.save(new ForumBeitrag("Suche Mitfahrer*in von Hamburg nach Bremen", "Jan", "Moin! Fahre am 20.11.2019 mit dem Rad von Hamburg nach Bremen und suche noch nette Begleitung."));
                forumBeitragRepository.save(new ForumBeitrag("Fahrradflohmarkt jeden Samstag!", "Laura", "Wir machen jeden Samstag Fahrradflohmarkt in der HoFa in Wilhelmsburg. Kommt vorbei!"));
                commentRepository.save(new Comment("Cool!", 2));

            }


        }

}
