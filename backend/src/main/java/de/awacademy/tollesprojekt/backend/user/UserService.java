package de.awacademy.tollesprojekt.backend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public boolean usernameExists(String username) {
        return userRepository.existsByUsernameIgnoreCase(username);
    }

    /**
     * Register a new User
     *
     * @param username The username
     * @param password The plaintext password
     * @return The newly saved User
     */
    public User register(String username, String email, String password) {
        String encodedPassword = passwordEncoder.encode(password);
        return userRepository.save(new User(username, email, encodedPassword));
    }

    //    public User registerAdmin(String username, String password) {
//        String encodedPassword = passwordEncoder.encode(password);
//        User admin = new User(username, encodedPassword);
//        admin.setAdmin(true);
//        return userRepository.save(admin);
//    }
    public User findByUsernameIgnoreCase(String username) {
        User user = userRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        return user;
    }

    public Optional<User> findById(long id) {
        return userRepository.findById(id);
    }
}
