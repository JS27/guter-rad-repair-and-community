package de.awacademy.tollesprojekt.backend.forum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
public class ForumBeitragController {

    @Autowired
    private ForumBeitragRepository forumBeitragRepository;

    @GetMapping("/api/forum")
    public List<ForumBeitragDTO> lesen() {
        List<ForumBeitragDTO> response = new LinkedList<>();

        for (ForumBeitrag beitrag : forumBeitragRepository.findAll()) {
            response.add(new ForumBeitragDTO(beitrag.getTitle(), beitrag.getName(), beitrag.getText(), beitrag.getId()));
        }

        return response;
    }

    @PostMapping("/api/forum")
    public List<ForumBeitragDTO> schreiben(@RequestBody ForumBeitragDTO forumBeitragDTO) {
        forumBeitragRepository.save(new ForumBeitrag(forumBeitragDTO.getTitle(), forumBeitragDTO.getName(), forumBeitragDTO.getText()));
        return lesen();
    }

    @PostMapping("/api/forum/delete")
    public String beitragLoeschen(@RequestBody long id) {
        ForumBeitrag forumBeitrag = forumBeitragRepository.findById(id).get();
        forumBeitragRepository.delete(forumBeitrag);
        return "redirect:/api/forum";
    }

}

