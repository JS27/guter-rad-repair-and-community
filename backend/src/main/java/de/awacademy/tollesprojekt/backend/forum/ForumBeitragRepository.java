package de.awacademy.tollesprojekt.backend.forum;

import org.springframework.data.repository.CrudRepository;


import java.util.List;

public interface ForumBeitragRepository extends CrudRepository<ForumBeitrag, Long> {
    List<ForumBeitrag>findAll();

}
