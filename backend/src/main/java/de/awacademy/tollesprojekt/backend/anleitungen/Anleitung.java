package de.awacademy.tollesprojekt.backend.anleitungen;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Anleitung {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;
    private String imgURL;

    @Column(length=100000)
    private String text;
    private Instant createdAt;
    private String author;

    public Anleitung() {
    }

    public Anleitung(String title, String imgURL, String text, String author) {
        this.title = title;
        this.imgURL = imgURL;
        this.text = text;
        this.author = author;
    }

    public Anleitung(String title, String imgURL, String text) {
        this.title = title;
        this.imgURL = imgURL;
        this.text = text;
    }

    public Anleitung(String title, String imgURL, String text, Instant createdAt, String author) {
        this.title = title;
        this.imgURL = imgURL;
        this.text = text;
        this.createdAt = createdAt;
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

}
